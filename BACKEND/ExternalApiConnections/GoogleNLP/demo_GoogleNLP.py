from BACKEND.ExternalApiConnections.GoogleNLP.ApiClient import GoogleCloudLanguageClient

google_nlp = GoogleCloudLanguageClient()


def test_text_sentiment():
    text = "This is bad"
    print(f"\nText sentiment -> {text}")
    google_nlp.analyze_text_sentiment(text)


def test_text_entities():
    text = "Guido van Rossum is great, and so is Python!"
    print(f"\nText entities -> {text}")
    google_nlp.analyze_text_entities(text)


def test_text_syntax():
    text = "Guido van Rossum is great!"
    print(f"\nText syntax -> {text}")
    google_nlp.analyze_text_syntax(text)


if __name__ == "__main__":
    test_text_sentiment()
    test_text_entities()
    test_text_syntax()
    # pass
