## System wsparcia pracy zespołów HelpDesk przez generowanie inteligentnych sugestii na zapytania w języku naturalnym.

# Autorzy
- Daniel D (@Rivijczyk98) 
- Magdalena S (@saw.magdalena)


# Technologia
* Vue3
* Python
* GoogleCloud Api 
* Technologia Cloudowa

# Założenia

1. System jest swego rodzaju interfejsem do wyszukiwania informacji w serwisach dokumentowych MPA (Multi Page Application) lub bazie wiedzy (serwisy z dokumentami i plikami klasy Wikipedia);
1. Użytkownik może zadawać pytanie w języku naturalnym; System próbuje wykorzystać API do wyszukiwania informacji po rozpoznanych słowach kluczowych z pytania (przypadek 1) lub znaleźć odpowiedź na pytanie potraktowane jako pytanie w języku naturalnym (przypadek 2);
1. Do obu funkcji powinny zostać wykorzystane API przetwarzania języka naturalnego NLP (Natural Language Processing) i NLU (Natural Language Understanding);
1. System powinien umożliwiać prezentację odpowiedzi na różne sposoby:
1. Podstawowa funkcjonalność (przypadek 1) obejmuje odszukanie listy pasujących wyników do wyszukiwania (na podstawie słów kluczowych wyciągniętych z pytania); Przy kliknięciu na każdym wyniku pojawia się podgląd znalezionego fragmentu (w treści strony/dokumentu) z zaznaczeniem tekstu, który najlepiej pasuje do wyszukiwania;
1. Implementacja przypadku 2 powinna być zbudowana na bazie API, które daje możliwość generowania odpowiedzi w języku naturalnym; lub nawet propozycji tych odpowiedzi; 
1. Wykonać eksperymenty, przetestować i zaimplementować silnik odpowiedzi na jednym wybranym API, jednego z głównych dostawców rozwiązań chmurowych AI;

# Backend project
## Technologia
Python + flask api do komunikacji
## Zewnętrzne api
1. Google Cloud - Natural Language AI
## Instrukcje dla developerów
1. Google Cloud:
* For Google Natural Language client to work properly,
there must be environmental variable 'GOOGLE_APPLICATION_CREDENTIALS'
containing path to <i>key.json</i> file with google cloud project api credentials

# Dane i narzędzia

1. NLP/NLU - Build Apps with Natural Language Processing (NLP) | IBM Watson | IBM, https://www.ibm.com/watson/natural-language-processing
1. NLP/NLU - Intelligent Virtual Agent - IBM Watson Assistant | IBM, https://www.ibm.com/cloud/watson-assistant
1. Question and Answer Maker, QnA Maker API | Microsoft Azure, https://azure.microsoft.com/en-us/services/cognitive-services/qna-maker/#overview
1. IBM Watson Natural Language Understanding - FAQ | IBM, https://www.ibm.com/cloud/watson-natural-language-understanding/faq
1. How to develop a self-learning chatbot with IBM Watson – the best conversational engine, https://action.bot/blog/self-learning-chatbot/
1. Conversational AI and Chatbots - Amazon Lex - Amazon Web Services, https://aws.amazon.com/lex/
1. Cloud Natural Language  |  Google Cloud, https://cloud.google.com/natural-language
1. Google AI Blog: Natural Questions: a New Corpus and Challenge for Question Answering Research, https://ai.googleblog.com/2019/01/natural-questions-new-corpus-and.html
